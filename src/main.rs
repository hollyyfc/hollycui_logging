use lambda_runtime::{Error, LambdaEvent, service_fn, Context};
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};
use tracing_subscriber::fmt::format::FmtSpan;
use tracing::{info, instrument};

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_span_events(FmtSpan::CLOSE)
        .init();

    let func = service_fn(func);
    lambda_runtime::run(func).await?;
    Ok(())
}

#[derive(Debug, Deserialize)]
struct CustomEvent {
    num1: i64,
    num2: i64,
}

#[derive(Serialize)]
struct CustomOutput {
    sum: i64,
}

#[instrument]
async fn func(event: LambdaEvent<CustomEvent>) -> Result<CustomOutput, Error> {
    let (event, _context) = event.into_parts();
    info!("Received event: {:?}", event);
    let sum = event.num1 + event.num2;
    Ok(CustomOutput { sum })
}

#[cfg(test)]
mod tests {
    use super::*;

    #[tokio::test]
    async fn test_lambda_function() {
        let test_event = CustomEvent {
            num1: 5,
            num2: 3,
        };
        let context = Context::default();

        let response = func(LambdaEvent::new(test_event, context)).await;
        assert!(response.is_ok());
        let output = response.unwrap();
        assert_eq!(output.sum, 8);
    }
}
