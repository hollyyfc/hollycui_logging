[![pipeline status](https://gitlab.com/hollyyfc/hollycui_logging/badges/main/pipeline.svg)](https://gitlab.com/hollyyfc/hollycui_logging/-/commits/main)

# 🔍 Holly's Lambda Function with Logging and Tracing 

## Description

This project demonstrates the integration of logging and tracing within a [Rust](https://www.rust-lang.org/)-based [AWS Lambda](https://aws.amazon.com/lambda/) function, aiming to provide a simplistic yet meaningful example of how to enhance observability in serverless applications. The core functionality of this Lambda function is to perform a basic arithmetic operation - specifically, adding two numbers provided as input and output the sum. To achieve a high level of observability, the project incorporates the `tracing` and `tracing-subscriber` crate for structured, event-based logging, and integrates with [AWS X-Ray](https://aws.amazon.com/xray/) for insightful tracing of function invocations. This allows for a detailed examination of the function's performance and behavior. Moreover, by leveraging [AWS CloudWatch](https://aws.amazon.com/cloudwatch/), all logs and traces are centralized, offering a unified view of the function's operational metrics. 

To send an invoke request, the user can either configure locally and invoke remotely:
```
cargo lambda invoke --remote mini6 --data-ascii '{ \"num1\": 25, \"num2\": 25 }'
```
or invoke directly through the API URL: 
```
curl -X POST https://lnnu9azlaj.execute-api.us-east-1.amazonaws.com/final-mini6 \
  -H 'content-type: application/json' \
  -d '{ "num1": 25, "num2": 30 }'
```

## Demo

- **Local Test Logging & Tracing**

During the local testing stage, both a deliberately incorrect invoke and a correct command have been sent to check the logging's functionality. The images below show that the logs are correctly output with `INFO` if the invoke is successful, otherwise an `ERROR` will be caught. 

![local1](https://gitlab.com/hollyyfc/hollycui_logging/-/wikis/uploads/ff3754463cbf97343fed91630ccf16fa/local1.png)

![local2](https://gitlab.com/hollyyfc/hollycui_logging/-/wikis/uploads/c54919f8aaef806d09e7fbc49ba77304/local2.png)

- **CloudWatch & AWS X-Ray Tracing**

Two separate invokes, one through the local remote invoke and the other directly through AWS Lambda console, have been tested below. CloudWatch centralizes the invoke information with the traces ID that could be individually inspected later. 

![xray1](https://gitlab.com/hollyyfc/hollycui_logging/-/wikis/uploads/81eb7331d773eb8108b2767e2b2d54e1/xray1.png)

![xray2](https://gitlab.com/hollyyfc/hollycui_logging/-/wikis/uploads/5384342262e1d6bfdf55cdfb2c14d9c9/xray2.png)

- **Trace-level Loggings**

If we click into one of the traces listed on AWS X-Ray, specific timelines and a complete log will be exhibited, integrating the functionality we built into our Rust function through crates `tracing` and `tracing-subscriber`. 

![trace1](https://gitlab.com/hollyyfc/hollycui_logging/-/wikis/uploads/175c385d849253658e36804c6e6ca9ac/trace1.png)

![trace2](https://gitlab.com/hollyyfc/hollycui_logging/-/wikis/uploads/97617a113e983da68750c04f886f2810/trace2.png)


## Steps Walkthrough

- **Build Cargo Project**
    - `cargo lambda new <YOUR-PROJECT-NAME>` in desired directory
    - Build Rust Lambda functions in `src/main.rs` 
    - Add `tracing`, `tracing-subscriber`, and relevant dependencies to `Cargo.toml` by `cargo add <dependency-name>` (This will attach the correponding versions automatically!)
    - `cargo lambda watch` for an initial function-build test
    - `cargo lambda invoke --data-ascii "{ \"num1\": \"25\", \"num2\": \"25\" }"` for local invoke
- **AWS Configuration**
    - AWS IAM -> Users -> Create user -> Attach policies: 
        - `AWSLambda_FullAccess` 
        - `IAMFullAccess`
        - `AWSXrayFullAccess`
    - Open the new user page: Create access key -> Save `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, and `AWS_REGION` (preferrably the same as your previous lambda functions' region)
    - Store AWS config details in a `.env` file in the root of the project
    - Export the above details by `export AWS_ACCESS_KEY_ID=abcdefg` in a terminal
- **Connect to AWS**
    - `cargo lambda build --release`
    - `cargo lambda deploy`
    - Go to AWS Lambda -> Check the function is there!
- **Check Logging and Tracing**
    - Open the Lambda function page
    - Configuration tab -> Monitoring and operations tools -> Additional monitoring tools -> Edit:
        - Open AWS X-Ray to Active tracing
        - Open CloudWatch Lambda Insights to Enhanced monitoring
    - Test remote invoke: `cargo lambda invoke --remote mini6 --data-ascii '{ \"num1\": 25, \"num2\": 25 }'` or direct invoke through the Test tab on the function's page
    - Monitor tab -> View X-Ray traces
    - Explore other features that CloudWatch centralizes! 
- **Deploy with API**
    - AWS API Gateway -> Create a new API (REST API)
    - Inside the new API: Create resource
    - Inside the new resource: 
        - Create method
        - Method type: `ANY`
        - Integration type: `Lambda function`
        - Lambda proxy integration: off
        - Lambda function: choose the (Region, Function ARN)
    - Stages: Create stage -> Deploy -> Find invoke URL (mine: https://lnnu9azlaj.execute-api.us-east-1.amazonaws.com/final-mini6)
    - Test API invoke: use the `curl` command above




